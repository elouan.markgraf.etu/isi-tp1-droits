#!/bin/bash

echo "//TEST SUR LE DOSSIER A//"

echo " "
cd dir_a
echo "L'admin peut-il lire un fichier du groupe A ?"
cat fichA.txt

echo " "
echo "L'admin peut-il modifier un fichier du groupe A ?"
echo "modification de l'admin" >> fichA.txt
echo "Est ce que le fichier s'est bien mis à jour ?"
cat fichA.txt

echo " "
echo "L'admin peut-il ajouter un fichier au dossier du groupe A ?"
ls
touch fichAdmin.txt
ls

echo " "
echo "L'admin peut-il renommer un fichier du groupe A ?"
mv fichAdmin.txt fichAdmin_rename.txt
ls

echo " "
echo "L'admin peut-il supprimer un fichier du groupe A ?"
rm fichAdmin_rename.txt
ls

echo " "
echo "L'admin peut-il ajouter un repertoire au groupe A ?"
mkdir dir_Admin
ls

echo " "
echo "L'admin peut-il supprimer un repertoire au groupe A ?"
rm -r dir_Admin
ls
cd ..


echo " "
echo "//TEST SUR LE DOSSIER B//"

echo " "
cd dir_b
echo "L'admin peut-il lire un fichier du groupe B?"
cat fichB.txt

echo " "
echo "L'admin peut-il modifier un fichier du groupe B ?"
echo "modification de l'admin" >> fichB.txt
echo "Est ce que le fichier s'est bien mis à jour ?"
cat fichB.txt

echo " "
echo "L'admin peut-il ajouter un fichier au dossier du groupe B ?"
ls
touch fichAdmin.txt
ls

echo " "
echo "L'admin peut-il renommer un fichier du groupe B ?"
mv fichAdmin.txt fichAdmin_rename.txt
ls

echo " "
echo "L'admin peut-il supprimer un fichier du groupe B ?"
rm fichAdmin_rename.txt
ls

echo " "
echo "L'admin peut-il ajouter un repertoire au groupe B ?"
mkdir dir_Admin
ls

echo " "
echo "L'admin peut-il supprimer un repertoire au groupe B ?"
rm -r dir_Admin
ls
cd ..


echo " "
echo "//TEST SUR LE DOSSIER C//"

echo " "
cd dir_c
echo "L'admin peut-il lire un fichier du groupe C?"
cat fichC.txt

echo " "
echo "L'admin peut-il modifier un fichier du groupe C ?"
echo "modification de l'admin" >> fichC.txt
echo "Est ce que le fichier s'est bien mis à jour ?"
cat fichC.txt

echo " "
echo "L'admin peut-il ajouter un fichier au dossier du groupe C ?"
ls
touch fichAdmin.txt
ls

echo " "
echo "L'admin peut-il renommer un fichier du groupe C ?"
mv fichAdmin.txt fichAdmin_rename.txt
ls

echo " "
echo "L'admin peut-il supprimer un fichier du groupe C ?"
rm fichAdmin_rename.txt
ls

echo " "
echo "L'admin peut-il ajouter un repertoire au groupe C ?"
mkdir dir_Admin
ls

echo " "
echo "L'admin peut-il supprimer un repertoire au groupe C ?"
rm -r dir_Admin
ls
cd ..

