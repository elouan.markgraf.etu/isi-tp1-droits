#!/bin/bash

echo "Peut lire les fichiers dans le dossier a?"
cd dir_a
cat fich1
cd ..

echo "Peut lire les fichiers dans le dossier c?"
cd dir_c
cat fich1

echo "On modifie le fichier dans le dossier c en ajoutant coucou"
echo "coucou" >> fich1 
echo "Cela à bien modifier?"
cat fich1

echo "Peut-on renommer un fichier dans le dossier c?"
mv fich1 fichtest
echo "Il y a t-il un fichier fichtest ?"
ls -al

echo "Peut-on effacer un fichier dans le dossier c?"
rm fich2
echo "Le fichier fich2 est t-il supprimer?"
ls -al

echo "Peut-on créer un fichier dans le dossier c?"
touch fichCreer
echo "Le fichier fichCreer est t-il créé?"
ls -al
cd ..

cd dir_a
echo "On modifie le fichier dans le dossier a en ajoutant coucou"
echo "coucou" >> fich1
echo "Cela à bien modifier?"
cat fich1

echo "Peut-on créer un nouveau dossier dans le dossier a?"
mkdir newDoss

echo "Peut-on créer un nouveau fichier dans le dossier a?"
touch fichTest

echo "Le fichier fichTest et le dossier newDoss on t-il bien été créé ?"
ls -al

echo "Peut-on supprimer un fichier dans le dossier a?"
rm fich1

echo "Le fichier fich1 a t-il été supprimer ?"
ls -al

echo "Peut-on renommer un fichier dans le dossier a?"
mv fich1 fichtest

echo "Il y a t'il un fichier fichtest ?"
ls -al
cd ..

cd dir_b

echo "Peut lire les fichiers dans le dossier b?"
cat fich

echo "On modifie le fichier dans le dossier b en ajoutant coucou"
echo "coucou" >> fich
echo "Cela à bien modifier?"
cat fich

echo "Peut-on supprimer un fichier dans le dossier b?"
rm fich

echo "Peut-on créer un nouveau fichier dans le dossier b?"
touch fichTest
echo "Il y a t-il un fichier fichTest ?"
ls -al
