#include <stdio.h>
#include <pwd.h>
#include <crypt.h>
#include <string.h>
#include <stdlib.h>
#include "check_pass.h"

int pwg(){
        FILE *f;
        int remplace = 0;
        struct passwd *pws;
        char buffer[80];
        char old_pass[30];
        char new_pass[30];
        char ch;
        int i = 0;
        int cpt =0;
        const char * salt = "$1$/iSaq7rB$EoUw5jJPPvAPECNaaWzMK/";
        f = fopen(PATH, "r+");
        if (f == NULL) {
                printf("Cannot open file");
                exit(EXIT_FAILURE);
        }
        pws = getpwuid(getuid());
        char * uid = pws->pw_name;
        while((ch=fgetc(f)) != EOF){
                if (ch == '\n'){
                        buffer[i]='\0';
                        cpt = i;
                        i = 0;
                        while (buffer[i] != '\0'){
                                if (buffer[i] == ':'){
                                        buffer[i] = '\0';
                                        break;
                                }
                                i++;
                        }
                        if(strcmp(buffer,uid)!=0){
                                memset(buffer, 0, 80);
                                i = 0;
                                continue;
                        }
                        else{
                                printf("Tapez votre ancien mdp");
                                scanf("%s", old_pass);
                                if(check_password(old_pass)==0){
                                        remplace = 1;
                                        printf("Tapez votre nouveau mdp");
                                        scanf("%s", new_pass);
                                        printf("%s", new_pass);
                                        cpt = cpt - i;
                                        fseek(f, -cpt, SEEK_CUR);
                                        fprintf(f, "%s\n", crypt(new_pass, salt));
                                        fclose(f);
                                        return 0;
                                }
                                else{
                                        printf("Mauvais mot de passe");
                                        return -1;
                                }
                        }
                }
                else{
                        buffer[i] = ch;
                        i++;
                }
        }
        if(remplace == 0){
                printf("Tapez votre mdp");
                scanf("%s", new_pass);
                fprintf(f, "%s:%s\n", uid, crypt(new_pass, salt));
        }
        fclose(f);
        return 0;
}
