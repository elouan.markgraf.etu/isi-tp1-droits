#include "check_pass.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
        const char * filename;
        filename = argv[1];
         if(argc<2){
                printf("Missing argument\n");
                exit(EXIT_FAILURE);
        }
        struct stat stats;
        if (stat(filename, &stats) == -1){
                exit(EXIT_FAILURE);
        }
        int size = 10;
        gid_t gr[10];
        int groupe;
        int meme_groupe = 0;
        groupe = getgroups(size, gr);
        for(int i=0; i<groupe; i++){
                 if(stats.st_gid == gr[i]){
                        meme_groupe = meme_groupe +1;
                 }
        }
        if(meme_groupe == 0){
                printf("Pas le même groupe");
                exit(EXIT_FAILURE);
        }
        if(access(filename, W_OK) != -1)
        {
                printf("Le fichier est trouvé et il peut être exécuté et peut effacer des fichiers dans le dossier par l'utilisateur");
        }
        else
        {
                printf("Le fichier n'est pas trouvé");
                exit(EXIT_FAILURE);
        }
        char password [20];
        printf("Saisir votre mot de passe : \n");
        scanf("%s",password);
        int bon_mdp;
        bon_mdp = check_password(password);
        printf("%d",bon_mdp);
        if (bon_mdp == 0){
                printf("Le fichier %s a été supprimé\n",filename);
                return remove(filename);
        }
