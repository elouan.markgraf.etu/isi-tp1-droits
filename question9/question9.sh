echo "Test avec lambda_a qui a déjà un mot de passe mais pas criptée"
sudo -u administrateur -s bash -c "touch dir_a/fichierTest"
echo "Vu du fichier passwd"
sudo -u administrateur -s bash -c "cd .. && cd administrateur && cat passwd"

echo "On change le mot de passe pour le crypté(baba)"
sudo -u lambda_a -s bash -c "./pwg"
echo
echo "Vu du fichier passwd après le cryptage"
sudo -u administrateur -s bash -c "cd .. && cd administrateur && cat passwd"
echo
echo "On crée un nouveau utilisateur"
sudo -u lambda_b -s bash -c "./pwg"
echo
echo "Vu du fichier passwd après création de lambda_b"
sudo -u administrateur -s bash -c "cd .. && cd administrateur && cat passwd"
echo
echo "On essaie de changer un mot de passe qui existe déjà"
echo
sudo -u lambda_b -s bash -c "./pwg"
echo
echo "Vu du fichier passwd après changement"
sudo -u administrateur -s bash -c "cd .. && cd administrateur && cat passwd"
echo
echo "Vu de tout les fichiers"
sudo -u lambda_a -s bash -c "cd dir_a && ls -al"
echo
echo "Test rmg"
sudo -u lambda_a -s bash -c "./rmg dir_a/fichierTest"
echo
echo "A t-il bien été supprimé"
sudo -u lambda_a -s bash -c "cd dir_a && ls -al"

