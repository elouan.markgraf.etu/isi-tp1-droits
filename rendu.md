# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Markgraf, Elouan, email: elouan.markgraf.etu@univ-lille.fr

- Hennebicq, Lucas, email: lucas.hennebicq.etu@univ-lille.fr

## Question 1

Le processus ne peut pas écrire sur le fichier. Puisque toto est celui qui a lancé le processus et il est aussi le propriétaire du fichier alors on regarde les 3 premiers tirets où il n'y a qu'un r donc il n'y a que la lecture qui est autorisé.

## Question 2

- Le caractère x pour un répertoire permet à un utilisateur d'accéder au répertoire.
- Quand on essaye de rentrer dans mydir avec toto, cela nous renvoie "Permission denied" puisque toto appartient au groupe ubuntu et ce groupe n'a plus l'acces à ce répertoire.
- En listant les fichiers on voit seulement le nom du fichier data.txt mais toutes les autres informations sont masqué puisque toto n'a pas les droits d'acces au répertoire mydir.

## Question 3

- Les ids donnent tous les 4 la valeur 1001 mais par contre toto ne peut pas ouvrir le fichier en lecture.

- Seul EUID a sa valeur qui a changé il est maintenant égale à 1000 et maintenant toto peut ouvrir et lire le fichier.

## Question 4

Les valeurs de EUID et de EGID sont égaux à 1001.

## Question 5

La commande chfn permet de modifier les informations des utilisateur contenu dans le fichier "etc/passwd". La commande ls -al /usr/bin/chfn affiche:
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn cela donne les permissions d'accès à ce fichier. C'est un fichier dans lequel le propriétaire peut écrire et lire. Les personnes qui font parti du groupe de l'utilisateur qui a créé le fichier et tous les autres utilsiateurs peuvent exécuter le fichier ainsi que le lire. Chaque utilisateur peut écrire dans le fichier puisque le SUID est activé(s).

## Question 6

Les mots de passe sont chiffrés et stockés dans le fichier /etc/shadow pour ne pas être modifiable par les utilisateurs.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Nous n'avons pas fait la question 10.








