#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    FILE *f;

    printf("UID : %d\n", getuid());
    printf("GID : %d\n", getgid());
    printf("EUID : %d\n", geteuid());
    printf("EGID : %d\n", getegid());

    f = fopen("./mydir/data.txt", "r");
    if (f == NULL) {
        printf("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File opens correctly\n");
    char str[20];
    while(fgets(str, 20, f) != NULL){
        puts(str);
    }

    fclose(f);
}