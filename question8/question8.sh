#!/bin/bash

echo "Test avec lambda_a"
sudo -u administrateur -s bash -c "touch dir_a/fichierTest"
echo "Vu de tout les fichiers"
sudo -u lambda_a -s bash -c "cd dir_a && ls -al"
echo "Test avec un mauvais mdp donc pas bababa"
echo
sudo -u lambda_a -s bash -c "./rmg dir_a/fichierTest"
echo
echo "Test dans le mauvais répertoire"
echo
sudo -u lambda_a -s bash -c "./rmg dir_b/fich2"
echo
echo "Test avec le bon mdp donc bababa"
echo
sudo -u lambda_a -s bash -c "./rmg dir_a/fichierTest"
echo
echo "A t-il bien été supprimé"
sudo -u lambda_a -s bash -c "cd dir_a && ls -al"

echo "Test avec lambda_b"
sudo -u administrateur -s bash -c "touch dir_b/fichierTest"
echo "Vu de tout les fichiers"
sudo -u lambda_b -s bash -c "cd dir_b && ls -al"
echo "Test avec un mauvais mdp donc pas bebe"
echo
sudo -u lambda_b -s bash -c "./rmg dir_b/fichierTest"
echo
echo "Test dans le mauvais répertoire"
echo
sudo -u lambda_b -s bash -c "./rmg dir_a/fich2"
echo
echo "Test avec le bon mdp donc bebe"
echo
sudo -u lambda_b -s bash -c "./rmg dir_b/fichierTest"
echo
echo "A t-il bien été supprimé"
sudo -u lambda_b -s bash -c "cd dir_b && ls -al"
