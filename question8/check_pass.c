#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "check_pass.h"
#include <pwd.h>


int check_password(char * password){
        char password_verif[20];
        FILE *f;
        int compare;
        char ch;
        struct passwd *pws;
        char buffer[50];
        int i = 0;
        f = fopen(PATH, "r");
        if (f == NULL) {
                printf("Cannot open file");
                exit(EXIT_FAILURE);
        }
        pws = getpwuid(getuid());
        char * uid = pws->pw_name;
        while((ch=fgetc(f)) != EOF){
                if (ch == '\n'){
                        buffer[i]='\0';
                        i = 0;
                        while (buffer[i] != '\0'){
                                if (buffer[i] == ':'){
                                        buffer[i] = '\0';
                                        break;
                                }
                                i++;
                        }
                        if(strcmp(buffer,uid)!=0){
                                memset(buffer, 0, 50);
                                i = 0;
                                continue;
                        }
                        else{
                                i++;
                                strcpy(password_verif,buffer+i);
                                compare = strcmp(password, password_verif);
                                if (compare == 0){
                                        printf("CORRECT PASSWORD\n");
                                        return 0;
                                }
                        }
                        memset(buffer,0,50);
                        i = 0;
                }
                else{
                        buffer[i] = ch;
                        i++;
                }
        }
        fclose(f);
        return -1;
}
